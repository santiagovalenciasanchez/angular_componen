import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent {



  datos: any = {};
  constructor(private http: HttpClient) { }
  private apiUrl = '';

  getDatos(): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'datos');
  }

  enviarDatos(datos: any): Observable<any> {
    return this.http.post<any>(this.apiUrl + 'datos', datos);
  }

  actualizarDatos(datos: any): Observable<any> {
    return this.http.put<any>(this.apiUrl + 'datos/' + datos.id, datos);
  }

  eliminarDatos(id: number): Observable<any> {
    return this.http.delete<any>(this.apiUrl + 'datos/' + id);
  }

  showModal(){
    Swal.fire({
      title: '<h2>Hemos tomado tu orden con exicto:</h2>',
      icon: 'success',
      html:
        'You can use <b>bold text</b>, '+
        'Nombre: [(Nombre)]Nombre.value <br>Correo: <p>[(Email)]</p> <br>Tel: {{Phone}} <br>Addrees: {{Address}} <br>Ciudad: {{City}}',
      timer: 10000,

      showCloseButton: false,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"</i>Cerrer ',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i>[(prueva)]',
      cancelButtonAriaLabel: ''


    })
  }



}
