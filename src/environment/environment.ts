
export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCKwersKT4SUCz1FjC-36J4xLHTEvzYa_g",
    authDomain: "TU_AUTH_DOMAIN",
    databaseURL: "TU_DATABASE_URL",
    projectId: "miveterinarioya-921bf",
    storageBucket: "TU_STORAGE_BUCKET",
    messagingSenderId: "TU_MESSAGING_SENDER_ID",
    appId: "miveterinarioya-921bf"
  }
};
